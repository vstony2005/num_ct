unit Main;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.StdCtrls,
  FMX.EditBox, FMX.NumberBox, FMX.Layouts, FMX.ListBox,
  FMX.Controls.Presentation, FMX.Edit,
  uNumCT, FMX.Menus, FMX.Effects;

type
  TForm1 = class(TForm)
    edtNumero: TEdit;
    lblDigit: TLabel;
    chkDisplayList: TCheckBox;
    btnListNumero: TButton;
    ListBox1: TListBox;
    lblDescription: TLabel;
    Layout1: TLayout;
    edtNbNumero: TNumberBox;
    GroupBox1: TGroupBox;
    Layout2: TLayout;
    PopupMenu1: TPopupMenu;
    mnCopy: TMenuItem;
    InnerGlowEffect1: TInnerGlowEffect;
    procedure btnListNumeroClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure chkDisplayListChange(Sender: TObject);
    procedure edtNumeroChangeTracking(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure mnCopyClick(Sender: TObject);
  private
    FCurrentHeight: Integer;
    FNum: TNumCT;
    procedure UpdateInfosNum(Sender: TObject);
  public
  end;

const
  HEIGHT_MIN = 175;

var
  Form1: TForm1;

implementation

uses
  FMX.Platform,
  FMX.Clipboard;

{$R *.fmx}

procedure TForm1.FormCreate(Sender: TObject);
begin
  FNum := TNumCT.Create;
  FNum.OnNumeroChangeEvent := UpdateInfosNum;

  FCurrentHeight := Height;
  Height := HEIGHT_MIN;

  edtNumero.Text := string.Empty;
  mnCopy.Enabled := False;
  InnerGlowEffect1.Opacity := 0;
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
  FNum.Free;
end;

procedure TForm1.FormResize(Sender: TObject);
begin
  if (chkDisplayList.IsChecked) then
    FCurrentHeight := Height;
end;

procedure TForm1.chkDisplayListChange(Sender: TObject);
begin
  if (chkDisplayList.IsChecked) then
    Height := FCurrentHeight
  else
    Height := HEIGHT_MIN;
end;

procedure TForm1.edtNumeroChangeTracking(Sender: TObject);
begin
  edtNumero.Text := AnsiUpperCase(edtNumero.Text);
  FNum.Number := edtNumero.Text;
end;

procedure TForm1.btnListNumeroClick(Sender: TObject);
var
  num: string;
  i: Integer;
begin
  ListBox1.Clear;

  if (FNum.State = TStateNumber.snValid) then
  begin
    num := FNum.Number;

    for i:=0 to Trunc(edtNbNumero.Value) do
    begin
      num := TNumCT.NextNumber(num);
      ListBox1.Items.Add(num);
    end;
  end;

  mnCopy.Enabled := (ListBox1.Count > 0);
end;

procedure TForm1.mnCopyClick(Sender: TObject);
var
  idx: Integer;
  clpBoard: IFMXClipboardService;
begin
  idx := ListBox1.ItemIndex;

  if (idx >= 0) then
  begin
    if (TPlatformServices.Current.SupportsPlatformService(IFMXClipboardService, clpBoard)) then
      clpBoard.SetClipboard(ListBox1.Items[idx]);
  end;
end;

procedure TForm1.UpdateInfosNum(Sender: TObject);
begin
  lblDescription.Text := FNum.ToString;

  if (FNum.State = TStateNumber.snValid) then
  begin
    lblDigit.Text := FNum.Digit;
    lblDigit.FontColor := TAlphaColorRec.Forestgreen;//Green;
    InnerGlowEffect1.GlowColor := TAlphaColorRec.Forestgreen;
    InnerGlowEffect1.Opacity := 0.9;
  end
  else if (FNum.IsFormatValid) then
  begin
    lblDigit.Text := FNum.Digit;
    lblDigit.FontColor := TAlphaColorRec.Darkred;
    InnerGlowEffect1.GlowColor := TAlphaColorRec.Darkred;
    InnerGlowEffect1.Opacity := 0.9;
  end
  else
  begin
    InnerGlowEffect1.Opacity := 0;
    lblDigit.Text := string.Empty;
  end;
end;

end.
