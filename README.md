# Check digit

Allows you to check the validity of a container number.

## Dev

FMX Project / Delphi 10.4

## Icons

Icons from https://materialdesignicons.com/.
