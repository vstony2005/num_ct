unit uNumCT;

interface

uses
  System.Classes;

type
  {$SCOPEDENUMS ON}
  TStateNumber = (snEmpty,snInvalidNumber,snInvalidDigit,snValid);

  TNumCT = class
  private
    FState: TStateNumber;
    FFullNumber: string;
    FLetters: string;
    FNumbers: Integer;
    FDigit: Char;
    FOnNumeroChangeEvent: TNotifyEvent;
    procedure SetNumber(const Value: string);
    procedure SetEmpty;
    class function IsValidFormat(const ANum: string): Boolean;
  public
    class function NextNumber(const ANum: string): string;
    class function CalculDigit(const ANum: string): Char;
    function IsFormatValid: Boolean;
    function ToString: string; override;
    property State: TStateNumber read FState;
    property Number: string read FFullNumber write SetNumber;
    property Digit: Char read FDigit;
  published
    property OnNumeroChangeEvent: TNotifyEvent read FOnNumeroChangeEvent write
        FOnNumeroChangeEvent;
  end;

implementation

uses
  System.SysUtils, System.Math,
  System.RegularExpressionsCore;

{ TNumCT }

class function TNumCT.CalculDigit(const ANum: string): Char;
var
  numbLetters, numbNumbers, char2num, pcId: string;
  sum, i, n: Integer;
begin
  Result := #0;

  if (not TNumCT.IsValidFormat(ANum)) then
  begin
    Result := #0;
    Exit;
  end;

  numbLetters := Copy(ANum, 1, 4);
  numbNumbers := Copy(ANum, 5, 6);
  char2num := '0123456789A?BCDEFGHIJK?LMNOPQRSTU?VWXYZ';
  pcId := numbLetters + numbNumbers;

  sum := 0;
  for i:=0 to 9 do
  begin
    n := char2num.IndexOf(pcId.Chars[i]);
    n := Trunc(n * Power(2, i));

    sum := sum + n;
  end;

  Result := ((sum mod 11) mod 10).ToString.Chars[0];
end;

function TNumCT.IsFormatValid: Boolean;
begin
  Result := FState in [TStateNumber.snValid, TStateNumber.snInvalidDigit];
end;

class function TNumCT.IsValidFormat(const ANum: string): Boolean;
var
  exp: TPerlRegEx;
  i: Integer;
begin
  Result := False;

  exp := TPerlRegEx.Create;
  try
    exp.Subject := ANum;
    exp.RegEx   := '^[A-Z]{4}[0-9]{6}[0-9]?$';

    Result := exp.Match;
  finally
    exp.Free;
  end;
end;

class function TNumCT.NextNumber(const ANum: string): string;
var
  newLetters, newNumbers, newNumb: string;
  numb: Integer;
begin
  Result := '';

  if (not TNumCT.IsValidFormat(ANum)) then
  begin
    Result := string.Empty;
    Exit;
  end;

  newLetters := Copy(ANum, 1, 4);
  newNumbers := Copy(ANum, 5, 6);

  if (newNumbers = '999999') then
    newNumbers := '000001'
  else
  begin
    numb := newNumbers.toInteger + 1;
    newNumbers := Format('%.6d', [numb]);

    newNumb := newLetters + newNumbers;

    Result := newNumb + TNumCT.CalculDigit(newNumb);
  end;
end;

procedure TNumCT.SetEmpty;
begin
  FLetters := string.Empty;
  FNumbers := 0;
  FDigit := #0;
  FState := TStateNumber.snEmpty;
end;

procedure TNumCT.SetNumber(const Value: string);
var
  num: string;
begin
  num := value.ToUpper.Trim;
  FFullNumber := num;

  if (num = string.Empty) then
    SetEmpty
  else if (not IsValidFormat(num)) then
  begin
    SetEmpty;
    FState := TStateNumber.snInvalidNumber;
  end
  else
  begin
    FLetters := Copy(num, 1, 4);
    FNumbers := Copy(num, 5, 6).ToInteger;
    FDigit := CalculDigit(Format('%s%.6d', [FLetters, FNumbers]));

    if (num.Length = 11) then
    begin
      if (num.Chars[10] = FDigit) then
        FState := TStateNumber.snValid
      else
        FState := TStateNumber.snInvalidDigit
    end
    else
      FState := TStateNumber.snValid;
  end;

  if Assigned(FOnNumeroChangeEvent) then
    FOnNumeroChangeEvent(Self);
end;

function TNumCT.ToString: string;
begin
  case FState of
    TStateNumber.snInvalidNumber:
      Result := 'Invalid number';
    TStateNumber.snInvalidDigit:
      Result := 'Invalid digit';
    TStateNumber.snValid: Result := Format('%s %.6d / %s',
                                           [FLetters, FNumbers, FDigit]);
  else
    Result := 'Empty number';
  end;
end;

end.
